def caesarCipher(s, k):
    # Write your code here
    string = ''
    k = k%26
    for x in s :
        x = ord(x)
        if 64<x<91 :
            if x+k > 90 :
                string += chr(64+(k-(90-x)))
            else :
                string += chr(x+k)
        elif 96<x<123 :
            if x+k > 122 :
                string += chr(96+(k-(122-x)))
            else :
                string += chr(x+k)
        else :
            string += chr(x)
    return string